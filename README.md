# AliTwitter
A very basic twitter clone with the following features:
- Create tweets
- Read tweets
- Delete tweets

## Environment Setup
- Ruby 2.6.5
- Bundler 2.1.4
- Rails 6.0.2.1

## System Dependencies
- [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
- [Vagrant](https://www.vagrantup.com/downloads.html)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
- [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [helm3](https://helm.sh/docs/intro/install/)

## Run Instructions

#### 1. Run kubernetes locally with minikube
```
minikube start --vm-driver='virtualbox'
```

#### 2. Modify the postgres chart values
Execute `kubectl cluster-info` and copy the kubernetes master's IP address, which is `192.168.99.100` in my development machine. 

Head over to the `alitwitter-chart/values.yaml` file.

The key that you have to modify would be the `postgres.host` key, and replace the default value of `192.168.99.100` with yours should they differ.

#### 3. Install the alitwitter kubernetes chart using helm
Install the chart with helm3:
```
helm install alitwitter alitwitter-chart
```

Execute `kubectl get pods -w` and monitor the pods until all their status are either RUNNING or COMPLETED.

#### 4. Run the service
Execute
```
minikube service svc-alitwitter
```

Or execute `minikube ip` and copy-paste the IP address with the port 31515 in your browser.

In my case, the URL would be `192.168.99.100:31515`

## CI/CD
To setup the CI/CD pipeline, refer to the [CI/CD documentation](CICD.md).
