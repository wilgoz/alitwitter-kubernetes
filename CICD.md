# CI/CD Setup
This section covers the steps required to setup the CI/CD pipeline for this project. Ensure that all the system dependencies covered in the README have been satisfied.

#### 1. Boot up the base virtual machine with Vagrant

```
vagrant up
```
The following virtual machine will be created:

| Name | IP Address |
|-|-|
| gitlab-runner | 192.168.50.100 |

#### 2. Prepare the gitlab-runner virtual machine
In the root project diretory, execute:
```
ansible-playbook -i provision/hosts/hosts.yml provision/deploy.yml
```
This will install docker and gitlab-runner to the gitlab-runner VM.

#### 3. SSH into the gitlab-runner VM
Establishing an SSH connection with the gitlab-runner VM can be done through Vagrant, as follows:
```
vagrant ssh gitlab-runner
```
Or manually assuming OpenSSH is installed in your local machine:
```
ssh -i .vagrant/machines/gitlab-runner/virtualbox/private_key vagrant@192.168.50.100
```

#### 4. Install helm3 and kubectl in the gitlab-runner VM
Execute the following commands:
```
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
```

#### 5. Access the kubernetes cluster from the gitlab-runner VM
Change the user to `gitlab-runner` and create a `~/.kube/config` file.
```
sudo su gitlab-runner
mkdir ~/.kube
vim ~/.kube/config
```

Copy-paste the following template to the config file:
```
apiVersion: v1
clusters:
- cluster:
    certificate-authority: /home/vagrant/ca.crt
    server: https://192.168.99.100:8443
  name: minikube
contexts:
- context:
    cluster: minikube
    user: minikube
  name: minikube
current-context: minikube
kind: Config
preferences: {}
users:
- name: minikube
  user:
    client-certificate: /home/vagrant/client.crt
    client-key: /home/vagrant/client.key
```

Modify the `cluster.server` value if your kubernetes master is running at a different IP address, which you can discover by running `kubectl cluster-info` in your local machine (not within the gitlab-runner vm).

You will need to create the following files in the `/home/vagrant/` directory:
- ca.crt
- client.crt
- client.key

In my local development machine, these files can be found in:

> These files may be located differently in your machine. Modify the paths accordingly.

| File | Path |
|-|-|
| ca.crt | /Users/willy.ghozali/.minikube/ca.crt |
| client.crt | /Users/willy.ghozali/.minikube/profiles/minikube/client.crt |
| client.key | /Users/willy.ghozali/.minikube/profiles/minikube/client.key |

Copy the contents of the above files to the related files that you just created in the `/home/vagrant` directory of the gitlab-runner VM.

Executing `kubectl get pods` in the gitlab-runner VM should list all the running pods within the cluster that is running on your local machine.

#### 6. Set up Docker and Shell executors in the gitlab-runner VM
> The step assumes that you have a fresh gitlab repository that will contain this project.

To register the runner to gitlab, execute:
```
sudo gitlab-runner register --docker-privileged
```

You will be prompted to provide several inputs. Follow the following input sequence to register the Docker executor:

1. https://gitlab.com
2. The gitlab-ci token can be found in your gitlab repository in `Settings > CI / CD > Runners`
3. build-runner
4. docker
5. docker
6. alpine:latest

Repeat the above steps to register the Shell executor with the following modifications (step 6 is no longer required):

3. deploy-runner
4. shell
5. shell

After registering the above executors, execute the following command to restart gitlab-runner:
```
sudo gitlab-runner restart
```

You may logout of the gitlab-runner VM at this stage.


#### 7. Set up the required docker details in your gitlab repository
In the `Settings > CI / CD > Variables` section of your gitlab repository,
set up the following variables:

| Type | Key | Value | State | Masked | Scope
|-|-|-|-|-|-|
| Variable | DOCKER_PASSWORD | { your docker password } | Disabled | Disabled | All environments
| Variable | DOCKER_USERNAME | { your docker username }  | Disabled | Disabled | All environments
| Variable | IMAGE_NAME | { the image name }  | Disabled | Disabled | All environments

#### 8. Modify the helm chart values
Head over to the `alitwitter-chart/values.yaml` file.

The key that you have to modify would be the `app.image` key. Replace the value with:
```
{ your docker hub username }/{ the image name you selected in step 7 }
```

#### 9. Push the project to your repo
The CI/CD pipeline should automatically run upon every pushes to the `master` branch.